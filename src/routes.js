import VueRouter from 'vue-router';
import Page from './views/Page.vue';
import Admin from './views/Admin.vue';


let routes = [{
    path: '/admin',
    component: Admin
}, {
    path: '/',
    component: Page
}, {
    path: '/:pageUrl*',
    component: Page
}];

export default new VueRouter({
    routes,
    linkActiveClass: 'is-active'
});