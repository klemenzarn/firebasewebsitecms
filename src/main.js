import Vue from 'vue'
import App from './App.vue'

// Initialize Firebase
var config = {
    apiKey: "AIzaSyDIbQgNxT4Vy7zR4P1d7t_4IfAxtTy4rdU",
    authDomain: "fir-cms-a650a.firebaseapp.com",
    databaseURL: "https://fir-cms-a650a.firebaseio.com",
    storageBucket: "fir-cms-a650a.appspot.com",
    messagingSenderId: "721485241843"
};
firebase.initializeApp(config);


var dummyData = {
    pages: {
        home: {
            title: 'home',
            content: 'home content'
        },
        docs: {
            title: 'docs',
            content: 'docs content'
        },
        about: {
            title: 'about',
            content: 'about content'
        }
    }

};
console.log(JSON.stringify(dummyData));

new Vue({
    el: '#app',
    render: h => h(App)
});