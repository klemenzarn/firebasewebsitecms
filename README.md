# FireBaseCMS

> publish free websites with firebase as your backend (hosting needed only for html, css and js files... Database and files such as images are on firebase service)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

